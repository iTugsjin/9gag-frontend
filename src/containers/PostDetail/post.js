import React from 'react';
import { Link } from 'react-router-dom';
import './style.css';

const PostDetail = () => (
  <div className="main-wrap">
    <section id="individual-post">
      <article className="post-page">
        <header>
          <h1 className="list-title">S2 Steve, best mom ever</h1>
          <p className="post-meta">
            <Link to="/" className="meta-link">
              <span>1234</span> оноо
            </Link>
            <span className="meta-clearfix">·</span>
            <Link to="/" className="meta-link">
              <span>2451</span> сэтгэгдэл
            </Link>
          </p>
          <div className="fixed-wrap-post-bar">
            <div className="post-after-a in-post-top">
              <div className="vote list-entry-buttons">
                <ul className="btn-vote left">
                  <li>
                    <Link to="/" className="up" />
                  </li>
                  <li>
                    <Link to="/" className="down" />
                  </li>
                </ul>
              </div>
              <div className="share">
                <ul>
                  <li>
                    <a href="/" className="btn-share facebook">Facebook</a>
                  </li>
                </ul>
              </div>
              <div className="post-nav">
                <Link to="/" className="next">
                  <span className="label">Next Post</span>
                  <span className="arrow" />
                </Link>
              </div>
            </div>
          </div>
          <div className="clearfix" />
        </header>
        <div className="post-container">
          <div className="image-post post-view">
            <img src="https://img-9gag-fun.9cache.com/photo/aDz5xLd_460s.jpg" alt="S2 Steve, best mom ever" />
          </div>
        </div>
      </article>
      <div className="post-comment">
        <div className="comment-list">
          <div className="tab-bar clearfix">
            <div className="tab-bar-left">
              <h3>329 сэтгэгдэл</h3>
            </div>
            <div className="tab-bar-right">
              <ul className="tab">
                <li className="active"><Link to="">Hot</Link></li>
                <li className=""><Link to="">Fresh</Link></li>
              </ul>
            </div>
          </div>
        </div>
        <div className="comment-embed">
          <div className="comment-box first">
            <div className="avatar">
              <div className="image-container">
                <a href="" target="_blank"><img src="https://accounts-cdn.9gag.com/media/default-avatar/1_0_100_v0.jpg" alt="guest" /></a>
              </div>
            </div>
            <div className="payload">
              <div className="textarea-container">
                <div>
                  <textarea placeholder="Write comments..." className="post-text-area focus" />
                </div>
              </div>
              <div className="action">
                <div className="rhs"><p className="word-count"><span>1000</span></p>
                  <a href="" className="cmnt-btn size-30 submit-comment">Post</a>
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>
          <div className="comment-entry">
            <div className="avatar">
              <div className="image-container">
                <a href="" target="_blank">
                  <img src="https://accounts-cdn.9gag.com/media/default-avatar/1_126_100_v0.jpg" alt="user avatar" />
                </a>
              </div>
            </div>
            <div className="payload">
              <div className="info">
                <p>
                  <Link to="/" className="username">
                    Qwerty
                  </Link>
                  <span className="time">2 өдөр</span>
                </p>
              </div>
              <div className="content">
                <p>Season 1: why is Nancy with Steve? Hes a douche</p>
                <div className="action">
                  <span>
                    <Link to="/reply" className="reply">
                      Reply
                    </Link>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="comment-entry">
            <div className="avatar">
              <div className="image-container">
                <a href="" target="_blank">
                  <img src="https://accounts-cdn.9gag.com/media/default-avatar/1_18_100_v0.jpg" alt="user avatar" />
                </a>
              </div>
            </div>
            <div className="payload">
              <div className="info">
                <p>
                  <Link to="/" className="username">
                    Qwerty
                  </Link>
                  <span className="time">2 өдөр</span>
                </p>
              </div>
              <div className="content">
                <p>Blizzard is the only company that for me, makes Cinematic so awesome that I get goosebumps everytime I see them</p>
                <div className="action">
                  <span>
                    <Link to="/reply" className="reply">
                      Reply
                    </Link>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>);

export default PostDetail;
