import React from 'react';
import { Link } from 'react-router-dom';
import './style.css';

const Home = () => (
  <div className="main-wrap">
    <section id="list-view">
      <article key="1">
        <header>
          <Link to="/post/1" key="" className="list-link">
            <h1 className="list-title">S2 Steve, best mom ever</h1>
          </Link>
        </header>
        <div className="post-container">
          <Link to="/" className="list-link">
            <img className="badge-item-img" src="https://img-9gag-fun.9cache.com/photo/aDz5xLd_460s.jpg" alt="S2 Steve, best mom ever" />
          </Link>
        </div>
        <p className="post-meta">
          <Link to="/" className="meta-link">
            <span>1234</span> оноо
          </Link>
          <span className="meta-clearfix">·</span>
          <Link to="/" className="meta-link">
            <span>2451</span> сэтгэгдэл
          </Link>
        </p>
        <div className="item-vote-container post-after-a">
          <div className="vote list-entry-buttons">
            <ul className="btn-vote left">
              <li>
                <Link to="/" className="up" />
              </li>
              <li>
                <Link to="/" className="down" />
              </li>
              <li>
                <Link to="/" className="comment" />
              </li>
            </ul>
          </div>
        </div>
        <div className="clearfix" />
      </article>
    </section>
    <section id="list-view">
      <article key="1">
        <header>
          <Link to="/post/2" key="" className="list-link">
            <h1 className="list-title">My Ex girlfriend made this for me....you'll always be the best that ever happened to me</h1>
          </Link>
        </header>
        <div className="post-container">
          <Link to="/post/2" className="list-link">
            <img className="badge-item-img" src="https://img-9gag-fun.9cache.com/photo/a1KjDY2_460s.jpg" alt="S2 Steve, best mom ever" />
          </Link>
        </div>
        <p className="post-meta">
          <Link to="/" className="meta-link">
            <span>1234</span> оноо
          </Link>
          <span className="meta-clearfix">·</span>
          <Link to="/" className="meta-link">
            <span>2451</span> сэтгэгдэл
          </Link>
        </p>
        <div className="item-vote-container post-after-a">
          <div className="vote list-entry-buttons">
            <ul className="btn-vote left">
              <li>
                <Link to="/" className="up" />
              </li>
              <li>
                <Link to="/" className="down" />
              </li>
              <li>
                <Link to="/" className="comment" />
              </li>
            </ul>
          </div>
        </div>
        <div className="clearfix" />
      </article>
    </section>
  </div>);

export default Home;
