import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import { Fresh, Home, Hot, PostDetail, Trend } from './';
import { HeaderComponent, SidebarComponent, RightSectinComponent } from '../components';

const App = () => (
  <div>
    <HeaderComponent />
    <SidebarComponent />
    <Switch>
      <div id="container">
        <div className="page">
          <Route exact path="/" component={Home} />
          <Route path="/fresh" component={Fresh} />
          <Route path="/hot" component={Hot} />
          <Route path="/post/:id" component={PostDetail} />
          <Route path="/trend" component={Trend} />
          <RightSectinComponent />
        </div>
      </div>
    </Switch>


  </div>);

export default App;
