import Fresh from './Fresh';
import Home from './Home';
import Hot from './Hot';
import PostDetail from './PostDetail';
import Trend from './Trend';

export {
  Fresh,
  Home,
  Hot,
  PostDetail,
  Trend,
};
