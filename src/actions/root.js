
import {
  BILGEE,
  REQUEST_POSTS, RECIEVE_POSTS, ERROR_POSTS,
} from '../constant/types';

import postData from '../constant/posts.json';

export const bilgee = value => async (dispatch) => {
  dispatch({
    type: BILGEE,
    harnowsh: value,
  });
};

export function fetchPosts() {
  return async (dispatch) => {
    const payload = {};
    dispatch({
      type: REQUEST_POSTS,
      payload,
    });
    try {
      // const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
      //   method: 'GET',
      // });
      // const data = await response.json();
      // if (!response.ok) {
      //   const message = data.message || data;
      //   if (data) throw message;
      // }
      dispatch({
        type: RECIEVE_POSTS,
        payload: postData,
      });
    } catch (error) {
      dispatch({
        type: ERROR_POSTS,
        message: error,
      });
    }
  };
}
