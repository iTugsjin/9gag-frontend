import React from 'react';
import { Link } from 'react-router-dom';
// import { NavLink } from 'react-router-dom';
import './style.css';

const SidebarComponent = () => (
  <div className="section-sidebar">
    <section>
      <header>
        <h3>Онцлох</h3>
      </header>
      <ul className="nav">
        <li>
          <Link className="selected side-link" to="/hot">
            <i className="icon hot" /> Hot
          </Link>
        </li>
        <li>
          <Link className="selected side-link" to="/trend">
            <i className="icon trending" />Trend
          </Link>
        </li>
        <li>
          <Link className="selected side-link" to="/fresh">
            <i className="icon fresh" />Fresh
          </Link>
        </li>
      </ul>
    </section>
    <section>
      <header>
        <h3>Ангилал</h3>
      </header>
      <ul className="nav">
        <li>
          <Link className="selected side-link" to="/hot">
            <i className="thumbnail">
              <img src="https://miscmedia-9gag-fun.9cache.com/images/thumbnail-facebook/1482203076.0883_u8yQU9_220x220.jpg" alt="animal" />
            </i>
            Амьтад
          </Link>
        </li>
        <li>
          <Link className="selected side-link" to="/anime">
            <i className="thumbnail">
              <img src="https://miscmedia-9gag-fun.9cache.com/images/thumbnail-facebook/1481537858.9056_aZAvYJ_220x220.jpg" alt="animal" />
            </i>
          Аниэм &amp; Манга
          </Link>
        </li>
        <li>
          <Link className="selected side-link" to="/ask9gag">
            <i className="thumbnail">
              <img src="https://miscmedia-9gag-fun.9cache.com/images/thumbnail-facebook/1464856777.129_sAsYpE_220x220.jpg" alt="animal" />
            </i>
          Асуулт Асуух уу ?
          </Link>
        </li>
      </ul>
    </section>
  </div>
);

export default SidebarComponent;
