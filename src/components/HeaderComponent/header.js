import React from 'react';
// import { NavLink } from 'react-router-dom';
import { Link } from 'react-router-dom';
import './style.css';


const HeaderComponent = () => (
  <section id="top-nav">
    <div className="nav-wrap">
      <Link to="/" className="logo" />
      <nav className="nav-menu">
        <ul className="secondary">
          <li>
            <Link to="/" className="top-menu">
              ТВ
            </Link>
          </li>
          <li>
            <Link to="/" className="top-menu">
              Сонирхолтой
            </Link>
          </li>
          <li>
            <Link to="/" className="top-menu">
              GIF
            </Link>
          </li>
          <li>
            <Link to="/" className="top-menu">
              Апп татах
            </Link>
          </li>
          <li>
            <Link to="/" className="top-menu">
              Бичлэгүүд
            </Link>
          </li>
        </ul>
      </nav>
      <div className="nav-right">
        <div className="search-section">
          <Link to="/search" className="search">
              Нэвтрэх
          </Link>
        </div>
        <div className="visitor-section">
          <Link to="/login" className="btn-mute">
              Нэвтрэх
          </Link>
          <Link to="/signup" className="btn-primary">
              Бүртгүүлэх
          </Link>
        </div>
      </div>
    </div>
  </section>);

export default HeaderComponent;
