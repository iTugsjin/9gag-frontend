import {
  REQUEST_POSTS, RECIEVE_POSTS, ERROR_POSTS,
} from '../constant/types';

const defaultState = {
  data: [],
  text: 'init text',
  posts: [],
  fetching: false,
};

const root = (state = defaultState, action) => {
  switch (action.type) {
    case REQUEST_POSTS:
      return {
        ...state,
        fetching: true,
      };
    case RECIEVE_POSTS:
      return {
        ...state,
        fetching: false,
        posts: action.payload,
      };
    case ERROR_POSTS:
      return {
        ...state,
        fetching: false,
      };
    default:
      return state;
  }
};

export default root;
